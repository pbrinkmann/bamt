#!/usr/bin/perl 

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.

eval
{
 require '/opt/bamt/common.pl';
 require '/opt/bamt/sendstatus.pl';
};

if ($@)
{
  die "\n\n\nWe were unable to find or process a core BAMT library:\n\n" .
    $@ . "\n\n\nThe BAMT tools cannot function until the above error is corrected.\n\nMaybe this happened";
}


$SIG{__DIE__} = sub { &handleDeath(@_); };

my $last_gpustatus = 0;

my $refresh_gpustatus = 30;

my $conf = &getConfig;
%conf = %{$conf};

my @mib = ( 
    [ '', \&nop ], 	
    ['1', \&nop ],
    ['2', \&nop ],
    ['2.1',\&nop ],
    
	['2.1.1',\&nop ],
	['2.1.1.0',\&settingStr ],
	['2.1.2',\&nop ],
	['2.1.2.0',\&settingStr ],
	['2.2',\&nop ],
	['2.2.1',\&nop ],
);

# gpuTable

our @pcigpu = &getPCIGPUdata;
our @gpustatus;
our @minerstatus;

for (my $id = 1;$id < 12;$id++)
{
	push(@mib, ['2.2.1.' . $id,\&nop ] );
	
	for (my $i = 1;$i <= @pcigpu;$i++)
	{
		push(@mib, ['2.2.1.' . $id . '.' . $i,\&gpuTable ] );
	}
}

for (my $id = 1;$id < 11;$id++)
{
	push(@mib, ['2.3.1.' . $id,\&nop ] );
	
	for (my $i = 1;$i <= @pcigpu;$i++)
	{
		push(@mib, ['2.3.1.' . $id . '.' . $i,\&gpuStatusTable ] );
	}
}

for (my $id = 1;$id < 14;$id++)
{
	push(@mib, ['2.4.1.' . $id,\&nop ] );
	
	for (my $i = 1;$i <= @pcigpu;$i++)
	{
		push(@mib, ['2.4.1.' . $id . '.' . $i,\&gpuMiningTable ] );
	}
}

$origin = ".1.3.6.1.4.1.5454";
$cmd = "";

#&blog("startup with origin $origin");

$| = 1;

while(<>)
{
	chomp();
	my $in = $_;
	
	
#	&blog("input: $in");
	
	if ($in eq "PING")
	{
#		&blog("ping -> pong");
		print "PONG\n";
	}
	elsif ($in eq "get")
	{
#		&blog("cmd set to get");
		$cmd = "get";
	}
	elsif ($in eq "getnext")
	{
#		&blog("cmd set to getnext");
		$cmd = "getnext";
	}
	elsif ($in =~ /^$origin\.(.*(\d+?$))/)
	{
	
		my $oid = $1;
		my $last = $2; 
		
		my $curindex = -1;
		
		for ($i = 0;$i<@mib;$i++)
		{
			if ($mib[$i][0] eq $oid)
			{
				$curindex = $i;
			}
		}
		
		if ($curindex == -1)
		{
#			&blog("not found $oid");
			print "NONE\n";
		}
		else
		{
				
			if ($cmd eq 'getnext')
			{
				$curindex++;
				
				while (($curindex < @mib) && ($mib[$curindex][1] == \&nop))
				{
					$curindex++;
				}
				
			}
				
			if ($curindex < @mib)
			{
#				&blog("for " . $origin . '.' . $mib[$curindex][0] . "  curindex $curindex\n"); 
		
				# get current data
				if (time - $last_gpustatus > $refresh_gpustatus)
				{
					@gpustatus = &getFreshGPUData(1);
					$last_gpustatus = time;
				}
				 
				print $origin . '.' . $mib[$curindex][0] . "\n";
			
				&{$mib[$curindex][1]}($mib[$curindex][0]);
			}
			else
			{
#				&blog("getnext sent us off the mib");
				print "NONE\n";
			}
		}
			
	}
}	


sub nop
{
}

sub settingStr
{
	
	my ($oid) = @_;
	
	print "string\n";
	
	if ($oid eq "2.1.1.0")
	{	
		print $conf{'settings'}{'miner_id'} . "\n";
	}
	elsif ($oid eq "2.1.2.0")
	{
		print $conf{'settings'}{'miner_loc'} . "\n";
	}
}


sub gpuTable
{
	my ($oid) = @_;
	
	
	my ($el,$gpu) = $oid =~ /^2\.2\.1\.(\d+?)\.(\d+?)/;
	
	$gpu--;
	
	
	if ($el == 1)
	{
		print "integer\n";
		print "$gpu\n";
	}
	elsif ($el == 2)
	{
		print "string\n";
		print "$gpustatus[$gpu]{'clid'}\n";
	}
	elsif ($el == 3)
	{
		print "string\n";
		print "$pcigpu[$gpu]{'pciid'}\n";
	}
	elsif ($el == 4)
	{
		print "string\n";
		print "$gpustatus[$gpu]{'display'}\n";
	}
	elsif ($el == 5)
	{
		print "string\n";
		print "$pcigpu[$gpu]{'device'}\n";
	}
	elsif ($el == 6)
	{
		print "string\n";
		print "$pcigpu[$gpu]{'vendor'}\n";
	}
	elsif ($el == 7)
	{
		print "string\n";
		print "$pcigpu[$gpu]{'sdevice'}\n";
	}
	elsif ($el == 8)
	{
		print "string\n";
		print "$pcigpu[$gpu]{'svendor'}\n";
	}
	elsif ($el == 9)
	{
		print "string\n";
		print "$gpustatus[$gpu]{'family'}\n";
	}
	elsif ($el == 10)
	{
		print "integer\n";
		print "$gpustatus[$gpu]{'cores'}\n";
	}
	
	elsif ($el == 11)
	{
		print "integer\n";
		if ( defined($conf{"gpu$gpu"}{disabled}) && ($conf{"gpu$gpu"}{disabled} == 1))
		{
			print "0\n";
		}
		else
		{
			print "1\n";
		}
		
	}
	
}




sub gpuStatusTable
{
	my ($oid) = @_;
	
	my ($el,$gpu) = $oid =~ /^2\.3\.1\.(\d+?)\.(\d+?)/;
	
	$gpu--;
	
	print "integer\n";
	
	if ($el == 1)
	{
		print "$gpustatus[$gpu]{current_core_clock}\n";
	}
	elsif ($el == 2)
	{
		print "$gpustatus[$gpu]{current_mem_clock}\n";
	}
	elsif ($el == 3)
	{
		print int($gpustatus[$gpu]{current_core_voltage} * 1000) . "\n";
	}
	elsif ($el == 4)
	{
		print "$gpustatus[$gpu]{current_performance_level}\n";
	}
	elsif ($el == 5)
	{
		print "$gpustatus[$gpu]{current_load}\n";
	}
	elsif ($el == 6)
	{
		if (isdigit ($gpustatus[$gpu]{fan_speed}) )
		{
			print "$gpustatus[$gpu]{fan_speed}\n";
		}
		else
		{
			print "-1\n";
		}
	}
	elsif ($el == 7)
	{
		if (isdigit($gpustatus[$gpu]{fan_rpm}))
		{
			print "$gpustatus[$gpu]{fan_rpm}\n";
		}
		else
		{
			print "-1\n";
		}
	}
	elsif ($el == 8)
	{
		print int($gpustatus[$gpu]{current_temp_0}) . "\n";
	}
	elsif ($el == 9)
	{
		print "$gpustatus[$gpu]{current_powertune}\n";
	}
	elsif ($el == 10)
	{
		if (defined($gpustatus[$gpu]{hardware_errors}))
		{
			print "$gpustatus[$gpu]{hardware_errors}\n";
		}
		else
		{
			print "-1\n";
		}
	}
	else
	{
	
		print "0\n";
	}
}



sub gpuMiningTable
{
	
	my ($oid) = @_;
	
	my ($el,$gpu) = $oid =~ /^2\.4\.1\.(\d+?)\.(\d+?)/;
	
	$gpu--;
	
	if ($el == 1)
	{
		print "string\n";
		print $gpustatus[$gpu]{status} . "\n";
	}
	elsif ($el == 2)
	{
		print "string\n";
		print "$gpustatus[$gpu]{miner}\n";
	}
	elsif ($el == 3)
	{
		print "string\n";
		print "$gpustatus[$gpu]{kernel}\n";
	}
	elsif ($el == 4)
	{
		print "string\n";
		print "$gpustatus[$gpu]{kernel_params}\n";
	}
	elsif ($el == 5)
	{
		print "integer\n";
		print "$gpustatus[$gpu]{shares_accepted}\n";
	}
	elsif ($el == 6)
	{
		print "integer\n";
		print "$gpustatus[$gpu]{shares_invalid}\n";
	}
	elsif ($el == 7)
	{
		print "integer\n";
		print "$gpustatus[$gpu]{shares_stale}\n";
	}
	elsif ($el == 8)
	{
		print "integer\n";
		print "$gpustatus[$gpu]{shares_other}\n";
	}
	elsif ($el == 9)
	{
		print "integer\n";
		print  int($gpustatus[$gpu]{hashrate} * 1000) . "\n";
	}
	elsif ($el == 10)
	{
		print "integer\n";
		print  int($gpustatus[$gpu]{hashrate} * 1000) . "\n";
	}
	elsif ($el == 11)
	{
		print "integer\n";
		if (defined($gpustatus[$gpu]{gpu_uptime}))
		{
			print "$gpustatus[$gpu]{gpu_uptime}\n";
		}
		else
		{
			print "-1\n";
		}
	}
	elsif ($el == 12)
	{
		print "string\n";
		print  "$gpustatus[$gpu]{pool_url}\n";
	}
	elsif ($el == 13)
	{
		print "integer\n";
		if (defined($gpustatus[$gpu]{last_share_time}))
		{
			print "$gpustatus[$gpu]{last_share_time}\n";
		}
		else
		{
			print "-1\n";
		}
	}
	
}


