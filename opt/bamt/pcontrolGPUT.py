#!/usr/bin/python

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.
#
#    pcontrol borrows/copies heavily from atitweak by Mark / mjmvisser 
#    https://bitcointalk.org/index.php?topic=25750.0
#    
#    If you like pcontrol, please show some love to atitweak:
#    1Kh3DsAhiu65EC7DFFHDGoGowAp5usQrCG
#


import Queue
import threading
import sys
import socket
import select 
import datetime
import time
import curses

class pcontrolGPUT( threading.Thread ):

	def __init__( self, pcontrol, stdscr, gpu, evtlog ):
		self.pcontrol = pcontrol
		self.scr = stdscr
		self.gpu = gpu
		self.evtlog = evtlog

		self.dat = { 'init': True, 'init_time': time.time() }
		
		self.pcontrol.setGPUData(self.gpu, self.dat)
		
		threading.Thread.__init__ ( self )
		self.setDaemon ( True )
	
	def getd( self, key ):
		if (key in self.dat.keys()):
			return self.dat[key]
		else:
			return None
		
		
	def setd( self, key, val, stime=None ):
		self.dat[key] = val
		
		if (stime is None):
			self.dat[key + "_time"] = time.time()
		else:
			self.dat[key + "_time"] = stime
		
		
	def hasd(self, key):
		if (key in self.dat.keys()):
			return True
		else:
			return False
	
	def getts( self ):
		return datetime.datetime.now().strftime("%I:%M:%S")
		
	def run ( self ):
		
		self.setd('accept',0.0,0)
		self.setd('reject',0.0,0)
		self.setd('ratio',0.0,0)
		self.setd('url',"unknown",0)
		self.setd('getwork_start',0.0,0)
		self.setd('getwork_delay',0.0,0)
		self.setd('queuesize',0.0,0)
		self.setd('defqueuesize',0.0,0)
		self.setd('hash',0.0,0)
		self.setd('stat',"n/a",0)
		self.setd('msg_to',"",0)
		self.setd('msg_from',"",0)
		self.setd('connected',False,0)
		self.setd('seen_accept', 0, 0)
		self.setd('seen_reject', 0, 0)
		self.setd('seen_getwork', 0, 0)
		self.setd('seen_gotwork', 0, 0)
		
		self.inputs = [ ]
		
		while(1):
			try:
				server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				
				server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
				
				server.connect(('127.0.0.1', 54545 + self.gpu))
				server.setblocking(0)
				
				self.setd('connected',True)
				
				self.inputs = [ server ] 
				
				while( self.getd('connected') == True ):
					
					try:
						msg = self.pcontrol.getOutQueue(self.gpu).get(False)
						self.setd('msg_to', msg)
						
					except Queue.Empty:
						# nop
						msg = None
					else:
						# send queued command to server
						server.sendall(msg + "\n")
						
					
					readable, writable, exception = select.select(self.inputs, [] , [], 1)
					
					for s in readable: 
						
						data = s.recv(1024) 
						
						if data == '':
							s.close()
							if (s in self.inputs):
								self.inputs.remove(s)
							self.setd('connected', False)
							
						else:
							# got data from mining client
							self.setd('msg_from',data)
									
							for line in data.splitlines():
							
								if line.startswith('mhs:'):
									self.setd('hash', float(line.strip('mhs:')) / 1000.0)
									
								elif line.startswith('hey:'):
									self.setd('url', line.partition(':')[2].split('|')[0].split('@')[-1].split(':')[0])
									self.setd('accept', float(line.partition(':')[2].split('|')[1]))
									self.setd('reject', float(line.partition(':')[2].split('|')[2]))
									
								elif line.startswith('shr:'):
									self.setd('lastshare' , line.split(':')[1] )
									naccept = float(line.split(':')[2])
									self.setd('reject',  float(line.split(':')[3]) )
									
									if (naccept > self.getd('accept')):
										self.setd('seen_accept', self.getd('seen_accept') + 1 )
										
										self.evtlog.append(self.getts() + ': GPU ' + str(self.gpu) + ' : ' + self.getd('lastshare') + ' accepted at ' + self.getd('url'))
									else:
										self.setd('seen_reject', self.getd('seen_reject') + 1 )
										self.evtlog.append(self.getts() + ': GPU '+ str(self.gpu) + ' : ' + self.getd('lastshare') + ' REJECTED at ' + self.getd('url'))
									
									self.setd('accept', naccept)
									
									if (naccept > 0):
										self.setd('ratio',  float(  self.getd('reject') / ( self.getd('accept') + self.getd('reject') ) * 100.0) )
										
								elif line.startswith('evt:'):
									self.setd('stat', line.partition(':')[2])
										
								elif line.startswith('get:'):
									self.setd('seen_getwork', self.getd('seen_getwork') + 1 )
									
									self.setd('url', line.partition(':')[2].split('|')[0].split('@')[-1].split(':')[0] )
									self.setd('queuesize' , int(line.partition(':')[2].split('|')[1]) )
									self.setd('defqueuesize', int(line.partition(':')[2].split('|')[2]) )
									
									self.setd('stat', 'GET')
									
								elif line.startswith('got:'):
									self.setd('seen_gotwork', self.getd('seen_gotwork') + 1 )
									
									self.setd('getwork_delay',  self.getd('seen_gotwork_time') - self.getd('seen_getwork_time') )
									self.setd('url', line.partition(':')[2].split('|')[0].split('@')[-1].split(':')[0] )
									self.setd('queuesize' , int(line.partition(':')[2].split('|')[1]) )
									self.setd('defqueuesize', int(line.partition(':')[2].split('|')[2]) )
									
									self.setd('stat', 'got')
									
								elif line.startswith('dbg:'):
									self.setd('msg_debug', line.partition(':')[2])
									self.evtlog.append(self.getts() + '  GPU ' + str(self.gpu) + ' : ' + line.partition(':')[2])
									
								elif line.startswith('msg:'):
									self.setd('msg_msg', line.partition(':')[2])
									self.evtlog.append(self.getts() + ': GPU ' + str(self.gpu) + ' : ' + line.partition(':')[2])
									
								elif line.startswith('con:'):
									if line == 'con:c':
										self.evtlog.append(self.getts() + ': GPU ' + str(self.gpu) + ' : Connected to ' + self.getd('url'))
										self.setd('stat', 'CON')
									elif line == 'con:d':
										self.evtlog.append(self.getts() + ': GPU ' + str(self.gpu) + ' : Disconnected from ' + self.getd('url'))
										self.setd('stat', 'DIS')
									elif line == 'con:r':
										self.evtlog.append(self.getts() + ': GPU ' + str(self.gpu) + ' : Retrying ' + self.getd('url'))
										self.setd('stat', 'TRY')
							
							if (self.pcontrol.getInMenu() == 0) & (self.pcontrol.getDisplayType() == 0):
							
								spr = 3
								if (self.pcontrol.getNumGPUs() > 3):
									spr = 2
							
								self.scr.addstr(4 + self.gpu*spr, 2, "%56s" % " ")
								self.scr.addstr(5 + self.gpu*spr, 2, "%56s" % " ")
								
							
								for i in range(0,self.pcontrol.getNumGPUs()):
									self.scr.addstr(4 + i*spr, 2, ">", curses.color_pair(8) | curses.A_BOLD)
								self.scr.addstr(4 + self.gpu*spr, 2, ">")
								
								self.scr.addstr(4 + self.gpu*spr, 3, "%.3f Mh/s  " % self.getd('hash'), curses.A_BOLD)
								self.scr.addstr(4 + self.gpu*spr, 17, "A/R: %d/%d %.2f%% " % ( self.getd('accept'), self.getd('reject'),  self.getd('ratio')))
								self.scr.addstr(4 + self.gpu*spr, 37, "Q: %d/%d " % (self.getd('queuesize'), self.getd('defqueuesize')))
								
								self.scr.addstr(5 + self.gpu*spr, 3, "%-4s%-51s" % ( self.getd('stat'), self.getd('url') ))
								self.scr.addstr(5 + self.gpu*spr, 37, "U: %.2f " % ( self.getd('seen_accept') / ( time.time() - self.getd('init_time') / 60 )))
								
								if self.getd('seen_accept') + self.getd('seen_reject') > 0:
									self.scr.addstr(5 + self.gpu*spr, 46, "E: %3.1f%% " % (float(self.getd('seen_getwork')) / float(self.getd('seen_accept') + self.getd('seen_reject')) * 100))
								
								if (self.getd('getwork_delay') != 0) and (self.getd('getwork_delay') < 86400):
									self.scr.addstr(4 + self.gpu*spr, 46, "D: %3.0fms  " % (self.getd('getwork_delay') * 1000))
								
								self.scr.refresh()	
								
							elif (self.pcontrol.getInMenu() == 0) and (self.pcontrol.getDisplayType() == 1):
								self.scr.addstr(4, 1, "Pools")
								self.scr.refresh()
								
							elif (self.pcontrol.getInMenu() == 0) and (self.pcontrol.getDisplayType() == 4) and (self.gpu == self.pcontrol.getCurrentGPU()):
								# gpu detail screen
								
								goff = 6
								
								for x in range(goff,goff+8):
									self.scr.addstr(x, 1, "%24s" % " ")
									
								self.scr.addstr(goff , 2, "Hash: %.3f Mh/s  " % self.getd('hash'), curses.A_BOLD)
								
								self.scr.addstr(goff+2, 2, " A/R: %d/%d " % ( self.getd('accept'), self.getd('reject')))
								self.scr.addstr(goff+3, 2, " Rej: %.2f%% " %  self.getd('ratio'))
								
								self.scr.addstr(goff + 4, 2, "Stat: %-4s " % self.getd('stat'))
								
								self.scr.addstr(goff+5, 2, " Que: %d/%d " % (self.getd('queuesize'), self.getd('defqueuesize')))
								
								if (self.getd('getwork_delay') != 0):
									self.scr.addstr(goff + 6, 2, "Dlay: %3.0fms " % (self.getd('getwork_delay') * 1000))
								
								if (self.getd('seen_accept') + self.getd('seen_reject')) > 0:
									self.scr.addstr(goff + 7, 2, " Eff: %3.1f%% " % (float(self.getd('seen_getwork')) / float(self.getd('seen_accept') + self.getd('seen_reject')) * 100))
								
								self.scr.addstr(goff + 8, 2, "Util: %.2f sh/m " % (float(self.getd('seen_accept')) /  (time.time() - float(self.getd('init_time')) )  / 60 ))
								
								gy = 4
								gx = 26
								self.scr.addstr(gy,   gx, " Current Pool: " + self.getd('url'))
								self.scr.addstr(gy+2, gx, "   Pools File: " + self.pcontrol.getGPUSetting(self.gpu, "pool_file"))
								self.scr.addstr(gy+3, gx, "Pool Strategy: " + "priority failover")
								
								self.scr.addstr(gy+4, gx, "       Kernel: " + self.pcontrol.getGPUSetting(self.gpu, "kernel"))
								
								ts =  self.pcontrol.getGPUSetting(self.gpu, "kernel_params")
								if (len(ts) > 35):
									ts = ts[:34] + ".."
								
								self.scr.addstr(gy+5, gx, "Kernel Params: " + ts)
								
								
								
								self.scr.refresh()	
					
						
					
			except Exception, e:
				# oops..
				self.evtlog.append(self.getts() + ': GPU : ERR ' + str(e))
				
				spr = 3
				if (self.pcontrol.getNumGPUs() > 3):
					spr = 2
				
				if (self.pcontrol.getInMenu() == 0) & (self.pcontrol.getDisplayType() == 0):
					self.scr.addstr(4 + self.gpu*spr, 3, "%39s" % " ")
					self.scr.addstr(5 + self.gpu*spr, 3, "%39s" % " ")
					self.scr.addstr(4 + self.gpu*spr, 3, 'ERR ' + str(e), curses.color_pair(1) | curses.A_BOLD)
					self.scr.refresh()
				
				if (server is not None):
					try:
						server.close()
						if (server in self.inputs):
							self.inputs.remove(server)
						self.setd('connected', False)
					except Exception, e:
						#dont care
						self.evtlog.append(self.getts() + ': GPU : ERR in close: ' + str(e))
			
			time.sleep(5)
